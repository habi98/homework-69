import React, {Fragment} from 'react';
import './Modal.css'

const Modal = props => {
    return (
        <Fragment>
            {props.show ? <div className="Modal">
                {props.children}
            </div>: null }
        </Fragment>
    );
};

export default Modal;