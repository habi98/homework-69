import React from 'react';
import {Badge, Button, Card, Col, ListGroup, ListGroupItem, Table} from "reactstrap";

const CardBasket = (props) => {


 let list = (
     <ListGroup>
     <ListGroupItem className="justify-content-between"> Доставка <Badge pill>{props.delivery}</Badge></ListGroupItem>
     <ListGroupItem className="justify-content-between">Итого  <Badge pill>{props.totalPrice}</Badge></ListGroupItem>
         <Button color="primary mt-2" onClick={props.showModal}>Place order</Button>
   </ListGroup>
 );
    return (
        <Col className="m-5">
            <Card className="p-4">
                <Table>
                    <tbody>
                    {Object.keys(props.order).map((dishes, key) => {
                        return(
                         <tr key={key}>
                            <td>{props.order[dishes].name}</td>
                            <td>x {props.order[dishes].amount}</td>
                            <td>{props.order[dishes].price} kgs</td>
                            <td> <Button close /></td>
                        </tr>
                        )
                    })}
                    </tbody>
                </Table>
                {props.show ? list :  <div>корзина заказа</div>}
            </Card>
        </Col>
    );
};

export default CardBasket;