import React, {Component} from 'react';
import {Col, Form, FormGroup, Input, Label} from "reactstrap";
import Button from "reactstrap/es/Button";
import {connect} from "react-redux";
import { createOrder} from "../../store/actions/orderMenu";


class FormOrder extends Component {
    state = {
       name: '',
       surname: '',
       email: '',
       postalCode: ''
    };


    valueChange = (event) => {
      const name = event.target.name;
      this.setState({[name]: event.target.value})
    };

    orderHandler = (event) => {
        event.preventDefault();
        const orderData = {
            customer: {...this.state},
            order: this.props.basket
        };

       this.props.createOrder(orderData)
    };



    render() {

        return (
            <Form onSubmit={this.orderHandler}>
            <FormGroup row>
                <Label sm={2}>Name</Label>
                <Col sm={10}>
                    <Input value={this.state.name} onChange={this.valueChange} type="text" name="name" />
                </Col>
            </FormGroup>
                <FormGroup row>
                    <Label  sm={2}>Surname</Label>
                    <Col sm={10}>
                        <Input value={this.state.surname} onChange={this.valueChange} type="text" name="surname" />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="exampleEmail" sm={2}>Email</Label>
                    <Col sm={10}>
                        <Input value={this.state.email} onChange={this.valueChange} type="email" name="email" />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="exampleEmail" sm={2}>Postal code</Label>
                    <Col sm={10}>
                        <Input value={this.state.code} onChange={this.valueChange} type="text" name="postalCode" />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col  sm={{size: 10, offset: 2}}>
                        <Button color="success">Clear order</Button>
                    </Col>
                </FormGroup>

            </Form>
        );
    }
}

const mapStateToProps = state =>({
    basket: state.card.basket
});


const mapDispachToProps = dispatch => ({
    createOrder: (order) => dispatch(createOrder(order))
});

export default connect(mapStateToProps,mapDispachToProps)(FormOrder);