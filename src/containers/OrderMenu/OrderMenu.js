import React, {Component} from 'react';
import {connect} from "react-redux";
import {addToCardBasket, getOrderMenu} from "../../store/actions/orderMenu";
import {Container, Button, Card, CardBody, CardColumns, CardImg, CardText, CardTitle, Col,  Row,} from "reactstrap";
import CardBasket from "../../components/CardBasket/CardBasket";
import Modal from "../../components/UI/Modal/Modal";
import FormOrder from "../FormOrder/FormOrder";


class OrderMenu extends Component {

    state = {
        show: false
    };

    showModal = () => {
        this.setState({show: true})
    };

    componentDidMount() {
        this.props.getOrderMenu();
    }

    render() {
        const card = Object.keys(this.props.dishes).map((dishes,index) => {
            return (
                <Card key={index}>
                    <CardImg top width="100%" src={this.props.dishes[dishes].image} alt="Card image cap" />
                    <CardBody>
                        <CardTitle>{this.props.dishes[dishes].name}</CardTitle>
                        <CardText>KGS {this.props.dishes[dishes].cost}</CardText>
                        <Button onClick={() => this.props.addToCard({name: this.props.dishes[dishes].name, price: this.props.dishes[dishes].cost})}>Add to card</Button>
                    </CardBody>
                </Card>
            )
        });
        return (
            <Container>
                <Modal show={this.state.show}>
                    <FormOrder/>
                </Modal>
            <Row>
             <Col sm={6} >
                <CardColumns  className="mt-5 p-2 border">
                    {card}
                </CardColumns>
             </Col>
              <CardBasket  showModal={this.showModal}
                    show={this.props.show}
                    order={this.props.basket}
                    totalPrice={this.props.totalPrice}
                    delivery={this.props.delivery}
              />
            </Row>
            </Container>
        );
    }
}

const mapStateToProps = state =>({
    dishes: state.dishes.dishes,
    basket: state.card.basket,
    totalPrice: state.card.totalPrice,
    delivery: state.card.delivery,
    show: state.card.show
});


const mapDispachToProps = dispatch => ({
    getOrderMenu: () => dispatch(getOrderMenu()),
    addToCard: (order) => dispatch(addToCardBasket(order))
});


export default connect(mapStateToProps, mapDispachToProps)(OrderMenu);