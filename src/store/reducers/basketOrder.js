import {ADD_TO_CARD} from "../actions/actionsType";

const dilivery = 150;



const initialState = {
    basket: [],
    totalPrice: dilivery,
    delivery: dilivery,
    show: false
};

const basketReducer = (state = initialState,  action) => {
    switch (action.type) {
        case ADD_TO_CARD:
            let order;
            if(state.basket[action.order.name]) {
                order = {...action.order, amount: state.basket[action.order.name].amount + 1};
            } else {
                order = {...action.order, amount: 1}
            }
            const orders = {...state.basket, [action.order.name]: order};
            return {
                ...state,
                basket: orders,
                totalPrice:  state.totalPrice + order.price ,
                show: true
            };
        default:
            return state
    }

};

export default basketReducer