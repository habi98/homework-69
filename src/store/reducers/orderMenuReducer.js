import {ORDER_MENU_SUCCESS, ORDER_REQUEST} from "../actions/actionsType";


const initialState = {
    dishes: {}
};

const orderMenuReducer = (state = initialState,  action) => {
     switch (action.type) {

         case ORDER_REQUEST :
             return{
                 ...state
             };
         case ORDER_MENU_SUCCESS:
             return {
                 ...state,
                 dishes: action.dishes
             };
         default:
             return state
     }

};

export default orderMenuReducer