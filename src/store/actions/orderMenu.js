import axios from '../../axios-dishes';
import {
    ADD_TO_CARD,
    ORDER_MENU_ERROR,
    ORDER_MENU_REQUEST,
    ORDER_MENU_SUCCESS,
    ORDER_REQUEST,
    ORDER_SUCCESS
} from "./actionsType";

export const orderMenuRequest = () => ({type: ORDER_MENU_REQUEST});
export const orderMenuSuccess = dishes => ({type: ORDER_MENU_SUCCESS, dishes});
export const orderMenuError = (error) => ({type: ORDER_MENU_ERROR, error});

export const orderRequest = () => ({type: ORDER_REQUEST});
export const ordersSuccess = () => ({type: ORDER_SUCCESS});

export const addToCardBasket = (order) => ({type: ADD_TO_CARD, order});

export const getOrderMenu = () => {
  return dispatch => {
      dispatch(orderMenuRequest());
      axios.get('/dishes.json').then(response => {
          dispatch(orderMenuSuccess(response.data))
      })
  }
};

export const createOrder = (order) => {
    return dispatch => {
        axios.post('/Order.json', order).then( response => {
           console.log(response)
        })
    }
};


export const addToCart = (order) => {
    return dispatch => {
        dispatch(addToCardBasket(order))
    }
};






